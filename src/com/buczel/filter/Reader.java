package com.buczel.filter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {
	static public String name;
	static public String day;
	static public String hour;
	static public int as;
	static public double avgRTT;
	static public int ipLength;
	static public double time;
	static public double downloadSpeed;
	
	static public String pattern1;
	static public String pattern2;
	
	static public int completition;
	
	static int[] size = {187, 1066, 5993};
	static int currentFile = 0;

	static String[] cities = { 
			"Australia",
			"Nowa Zelandia",
			"Singapur",
			"Nowa Kaledonia",
			"Tajlandia",
			"Korea",
			"Japonia",
			"Chiny",
			"Tajwan",
			"Turcja",
			"Indie",
			"Kenia",
			"Zimbabwe",
			"RPA",
			"Kanada - Hamilton",
			"USA - Madison",
			"USA - Boston",
			"USA - Honolulu",
			"USA - San Francisco",
			"Polska",
			"Rosja",
			"Grecja",
			"Niemcy",
			"Francja",
			"Norwegia",
			"Portugalia",
			"Wielka Brytania",
			"Brazylia",
			"Salwador",
			"Meksyk",
			"Argentyna",
			"Polinezja Francuska"
	};

	public static void main(String[] args) {
		for (currentFile = 0; currentFile < 3; currentFile++) {
		for (String city : cities) {
			ArrayList<String> list = new ArrayList<String>();
			List<Measurement> listOfMes = new ArrayList<Measurement>();
			try {
				String[] elements = readFile(city);
				for (String meas : elements) {
					extractData(list, meas);
					listOfMes.add(new Measurement(city, day, hour, as, avgRTT, ipLength, time, downloadSpeed));
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			saveResultsToFile(listOfMes, city);
			
			}
		}
	}

	private static void extractData(ArrayList<String> list, String meas) {
		getDate(meas);
		getASes(list, meas);
		getRTT(meas);
		getIpLength(meas);
		getTime(meas);
	}

	private static void saveResultsToFile(List<Measurement> listOfMes, String city) {
		for (Measurement singleMes : listOfMes) {
			try {
				String directoryName = "newFilteredFix" + size[currentFile];
			    File directory = new File(String.valueOf(directoryName));
			    if (! directory.exists()){
			        directory.mkdir();
			        // If you require it to make the entire directory path including parents,
			        // use directory.mkdirs(); here instead.
			    }
			    
				FileWriter fw = new FileWriter(directoryName + '/' + city + ".txt", true);
				fw.write(singleMes.getDay() + ' ' + singleMes.getHour() + ' ' + singleMes.getAs() + ' ' + singleMes.getAvgRTT() + ' '
						+ singleMes.getIpLength() + ' ' + singleMes.getDownloadSpeed() + ' ' + singleMes.getTime());																				
															
				fw.write(System.getProperty("line.separator"));
				
				System.out.println(city + " done.");
				fw.close();
			} catch (IOException e) {	
				System.out.println(e);

			}
		}
	}

	private static void getTime(String meas) {
		pattern1 = "!time!\n";
		pattern2 = "\n\n";
		Pattern timePattern = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2),
				Pattern.DOTALL);
		Matcher timeMatcher = timePattern.matcher(meas);

		while (timeMatcher.find()) {
			NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
			try {
			    Number number = format.parse(timeMatcher.group(1));
				time = number.doubleValue();
				downloadSpeed = size[currentFile]/time;
				
//				1066 KB, 187 KB, 5993 KB

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(meas);
			} 
		}
	}

	private static void getIpLength(String meas) {
		pattern1 = "60 byte packets\n";
		pattern2 = "!ping!";
		Pattern ipPattern = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2),
				Pattern.DOTALL);
		Matcher ipMatcher = ipPattern.matcher(meas);

		while (ipMatcher.find()) {
			ipLength = ipMatcher.group(1).split("\r\n|\r|\n").length;
		}
	}

	private static void getRTT(String meas) {
		pattern1 = "rtt min/avg/max/mdev = ";
		pattern2 = "ms";
		Pattern rttPattern = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2),
				Pattern.DOTALL);
		Matcher rttMatcher = rttPattern.matcher(meas);
		while (rttMatcher.find()) {
			Pattern rttPatternIns = Pattern.compile("/(.*?)/", Pattern.DOTALL);
			Matcher rttMatcherIns = rttPatternIns.matcher(rttMatcher.group(1));

			while (rttMatcherIns.find()) {
				avgRTT = Double.valueOf(rttMatcherIns.group(1));
			}

		}
	}

	private static void getASes(ArrayList<String> list, String meas) {
		pattern1 = "[";
		pattern2 = "]";
		Set<String> asSet = new LinkedHashSet<>(list);

		Pattern asPattern = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2),
				Pattern.DOTALL);
		Matcher asMatcher = asPattern.matcher(meas);
		while (asMatcher.find()) {
			asSet.add(asMatcher.group(1));
		}
		as = asSet.size();
	}

	private static void getDate(String meas) {
		Pattern datePattern = Pattern.compile("\\n([A-Za-z�����깜��ƥ�ʣ���ś]+)(,|�,) (\\d+)(.*), (\\d+)");
		Matcher dateMatcher = datePattern.matcher(meas);
		if (dateMatcher.find()) {
			day = getDayNumber(dateMatcher.group(1));
			hour = dateMatcher.group(5);
		}
	}

	private static String getDayNumber(String group) {
switch(group){
case "pon":
	return "0";
case "wto":
	return "1";
case "śro":
	return "2";
case "czw":
	return "3";
case "pi�":
	return "4";
case "sob":
	return "5";
case "nie":
	return "6";
default:
	return "error";
}
	}

	private static String[] readFile(String city) throws IOException {
		String contents = new String(Files.readAllBytes(Paths.get("files/" + size[currentFile] + '/'+ city + ".txt")));
		String[] elements = contents.split("!!!");
		return elements;
	}
}