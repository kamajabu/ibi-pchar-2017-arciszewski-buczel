package com.buczel.filter;

public class Measurement {
	
	
	private String name;
	private String day;
	private String hour;
	private int as;
	private double avgRTT;
	private int ipLength;
	private double time;
	private double downloadSpeed;
	
	
	public Measurement(String name, String day, String hour, int as, double avgRTT, int ipLength, double time,
			double downloadSpeed) {
		super();
		this.name = name;
		this.day = day;
		this.hour = hour;
		this.as = as;
		this.avgRTT = avgRTT;
		this.ipLength = ipLength;
		this.time = time;
		this.downloadSpeed = downloadSpeed;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDay() {
		return day;
	}


	public void setDay(String day) {
		this.day = day;
	}


	public String getHour() {
		return hour;
	}


	public void setHour(String hour) {
		this.hour = hour;
	}


	public int getAs() {
		return as;
	}


	public void setAs(int as) {
		this.as = as;
	}


	public double getAvgRTT() {
		return avgRTT;
	}


	public void setAvgRTT(double avgRTT) {
		this.avgRTT = avgRTT;
	}


	public int getIpLength() {
		return ipLength;
	}


	public void setIpLength(int ipLength) {
		this.ipLength = ipLength;
	}


	public double getTime() {
		return time;
	}


	public void setTime(double time) {
		this.time = time;
	}


	public double getDownloadSpeed() {
		return downloadSpeed;
	}


	public void setDownloadSpeed(double downloadSpeed) {
		this.downloadSpeed = downloadSpeed;
	}
	 
   
}
